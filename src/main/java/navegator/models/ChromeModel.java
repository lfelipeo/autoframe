package navegator.models;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeModel extends NavegatorModel {

	//private final String pathChromeConfigJson = getClass().getClassLoader().getResource("ChromeConfig.json")
			//.toExternalForm().toString();

	//private JsonObj chromejs = new JsonObj(new File(pathChromeConfigJson));

	public String getName() {
		return ("webdriver.chrome.driver");
	}

	public String getPath() {
		return ("Driver/chromedriver.exe");
	}

	@Override
	public ChromeDriver webDriver() {
		return new ChromeDriver();
	}

	@Override
	public void setPropriedades() {
		System.setProperty(getName(), getPath());
	}

}
