package tools;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class Javascript extends Selenium {

	/**
	 * JavascriptExecutor � respons�vel por executar todas os scripts em javascript.
	 */

	private JavascriptExecutor js = (JavascriptExecutor) getDriver();

	/**
	 * Espera o elemento estar vis�vel ent�o escreve o texto passado pro parametro
	 * usando Javascript.
	 * 
	 * @param id_campo
	 * @param texto
	 * @throws Exception
	 */

	public void escreveJsId(String id_campo, String texto) throws Exception {
		try {
			esperaElemento(By.id(id_campo));
			js.executeScript("document.getElementById('" + id_campo + "').setAttribute('value', '" + texto + "')");
		} catch (Exception e) {
			throw new Exception("ERRO AO ESCREVER NO ELEMENTO COM JAVASCRIPT. Javascript Erro: " + e.getMessage());
		}
	}

	/**
	 * Desce (scroll-down) toda a tabela ate o final.
	 * 
	 * @param id_table
	 */

	public void desceTabelaToda(String id_table) {
		js.executeScript(
				"var elem = document.getElementById('" + id_table + "'); " + "elem.scrollTop=elem.scrollHeight;");
	}

	public boolean esperaPaginaCarregarJs() {
		try {
			return js.executeScript("return document.readyState").equals("complete");
		} catch (Exception e) {
			return false;
		}
	}

	public void escreveNoConsoleDoJavascript(String texto) {
		try {

			js.executeScript("console.log('" + texto + "');");

		} catch (Exception e) {

		}
	}

	// -------------------------------- REFATORAR -//

	public void rolarPaginaJs(float coordenadaY) {
		js.executeScript("window.scrollBy(0, " + coordenadaY + ")");
	}

	public void fecharJanelaJs() {
		js.executeScript("window.close()");
	}

	// Usado!
	public ExpectedCondition<Boolean> waitForAjaxCalls() {

		try {
			return new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					return Boolean.valueOf(((JavascriptExecutor) driver)
							.executeScript("return (window.jQuery != null) && (jQuery.active === 0);").toString());
				}
			};
		} catch (Exception e) {
			return null;
		}
	}

	public void doisCliques(By by) throws Exception {
		try {
			WebElement element = getDriver().findElement(by);
			js.executeScript("arguments[0].doubleclick()", element);
		} catch (Exception e) {
			throw new Exception("ERRO AO EFETUAR UM DOIS CLIQUES. Javascript Ero: " + e.getMessage());
		}
	}
}
