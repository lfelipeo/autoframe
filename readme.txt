1 - Para gerar/atualizar um .jar deste projeto.

Dentro do Eclipse, clique com o bot�o direito no projeto e selecione: 
Run As > Maven Build... > No campo 'Goals' digitar: clean package

* Dessa forma um .jar ser� gerado na pasta 'target'.
* O .jar a ser usado � o que possui 'UBER' no nome. Esse � o jar que contem as dependencias do Maven.

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------

2 - Para ligar o .jar com um projeto novo deve-se:

2.1 - Criar uma pasta com o nome 'Driver' dentro do projeto que est� recebendo o .jar (no mesmo n�vel que a pasta src) e colocar os drivers do Chrome e IE nessa pasta.

2.2 - Colocar o .jar no mesmo n�vel da pasta acima e adicionar no path.

2.3 - Inst�ncia a classe 'BaseAutoFrame' e passar uma classe de Navegador.

Exemplo de classe:


public class BaseAuto {

	BaseAutoFrame frame = new BaseAutoFrame(new ChromeModel());
		
}


2.4 (Opcional) - Por boas pr�ticas deve-se criar uma classe, herda a a que inst�nciou a 'BaseAutoFrame' e criar m�todos getters para receber o driver (WebDriver) e o dsl.

Exemplo de classe:


public class BasePage extends BaseAuto {

	protected DSL getDsl() {
		return frame.getDSL();
	}

	protected WebDriver getDriver() {
		return frame.getDriver();
	}
}

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------

Seguindo todos os passos acima o projeto que recebe o .jar j� pode ser usado para criar automa��es.


