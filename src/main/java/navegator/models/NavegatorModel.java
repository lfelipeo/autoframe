package navegator.models;

import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class NavegatorModel {

	public String getName() {
		return null;
	}
	
	public String getPath() {
		return null;
	}
	
	public abstract RemoteWebDriver webDriver();
	
	public abstract void setPropriedades();
}
