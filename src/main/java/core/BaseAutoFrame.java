package core;

import driver.DriverFactory;
import navegator.models.NavegatorModel;

/**
 * Classe principal do Framework.
 * 
 * Essa classe deve ser instânciada em uma classe de configuração para execute
 * os testes.
 * 
 * @author LFO
 *
 */

public class BaseAutoFrame extends DriverFactory {

	public BaseAutoFrame(NavegatorModel nvg) {
		super(nvg);
	}
}
