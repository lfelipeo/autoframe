package tools;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import driver.DriverFactory;

public class Selenium extends DriverFactory{

	protected WebDriverWait wait = new WebDriverWait(getDriver(), 60);

	/**
	 * Localiza o elemento, espera at� que o elemento esteja clic�vel e v�sivel e
	 * clica.
	 * 
	 * @param by
	 * @throws Exception
	 */

	public void clica(By by) throws Exception {

		try {
			esperaElemento(by);
			getDriver().findElement(by).click();
		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NO ELEMENTO. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Espera o elemento estar vis�vel na tela.
	 * 
	 * @param by
	 * @throws Exception
	 */

	public void esperaElementoSerVisivel(By by) throws Exception {

		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO SER VIS�VEL. Selenium Erro: " + e.getLocalizedMessage());
		}

	}
 
	/**
	 * Espera o elemento estar clic�vel.
	 * 
	 * @param by
	 * @throws Exception
	 */

	public void esperaElementoSerClicavel(By by) throws Exception {

		try {
			getWait().until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO FOR CLIC�VEL. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Espera o elemento ser vis�vel e clic�vel.
	 * 
	 * @param by
	 * @throws Exception
	 */

	public void esperaElemento(By by) throws Exception {

		try {

			esperaElementoSerVisivel(by);
			esperaElementoSerClicavel(by);
			getDriver().findElement(by).isDisplayed();

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO SER CLICAVEL E VISIVEL. Selenium Erro: " + e.toString());
		}

	}

	/**
	 * Espera o elemento conter determinado valor.
	 * 
	 * @param by
	 * @param valor_esperado
	 * @return
	 * @throws Exception
	 */

	public void esperaValor(By by, String valor_esperado) throws Exception {

		WebElement element;

		try {
			element = getDriver().findElement(by);
		} catch (Exception e) {
			throw new Exception("ERRO AO LOCALIZAR O ELEMENTO. Selenium Erro: " + e.getMessage());
		}

		try {

			getWait().until(ExpectedConditions.textToBePresentInElementValue(element, valor_esperado));
			//Assert.assertEquals(valor_esperado, element.getAttribute("value"));

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O VALOR (" + valor_esperado + "). Selenium Erro: " + e.toString());
		}
	}

	/**
	 * Espera o elemento conter determinado texto.
	 * 
	 * @param by
	 * @param texto_esperado
	 * @throws Exception
	 */

	public void esperaTexto(By by, String texto_esperado) throws Exception {

		try {
			esperaElemento(by);
			//WebElement element = getDriver().findElement(by);
			getWait().until(ExpectedConditions.textToBePresentInElementLocated(by, texto_esperado));
			//Assert.assertEquals(texto_esperado, element.getText());
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O TEXTO (" + texto_esperado + "). Selenium Erro: " + e.toString());
		}
	}

	/**
	 * Pega o texto do elemento.
	 * 
	 * @param by
	 * @return
	 * @throws Exception
	 */

	public String pegaTexto(By by) throws Exception {

		try {
			esperaElemento(by);
			WebElement element = getDriver().findElement(by);
			return element.getText();
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O TEXTO DO ELEMENTO. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Pega o valor do elemento.
	 * 
	 * @param by
	 * @return O valor do elemento
	 * @throws Exception
	 */

	public String pegaValor(By by) throws Exception {

		try {
			esperaElemento(by);
			WebElement element = getDriver().findElement(by);
			return element.getAttribute("value");
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O VALOR DO ELEMENTO. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Verifica se o texto passado por parametro � o mesmo que se encontra em
	 * determinado elemento.
	 * 
	 * @param by
	 * @param texto
	 * @return O texto do elemento
	 * @throws Exception
	 */

	public boolean verificaCampoContemTexto(By by, String texto) throws Exception {
		esperaElemento(by);
		return getDriver().findElement(by).getText().contains(texto);
	}

	/**
	 * Verifica se em alguma c�lula da tabela contem um texto igual.
	 * 
	 * @param id_table
	 * @param elemento_busca
	 * @param coluna
	 * @throws Exception
	 */

	public void verificaTabelaTextoIgual(String id_table, String elemento_busca, int coluna) throws Exception {

		esperaElemento(By.id(id_table));

		List<WebElement> linhas = getDriver()
				.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + coluna + "]"));

		for (int i = 0; i <= linhas.size(); i++) {
			if (linhas.get(i).getText().equalsIgnoreCase(elemento_busca)) {
				break;
			}
		}
	}

	/**
	 * Verifica se em alguma c�lula contem um texto.
	 * 
	 * @param id_table
	 * @param elemento_busca
	 * @param coluna
	 * @throws Exception
	 */

	public void verificaTabelaContemTextoId(String id_table, String elemento_busca, int coluna) throws Exception {

		esperaElemento(By.id(id_table));

		List<WebElement> linhas = getDriver()
				.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + coluna + "]"));

		for (int i = 0; i <= linhas.size(); i++) {
			if (linhas.get(i).getText().toUpperCase().contains(elemento_busca.toUpperCase())) {
				break;
			}
		}
	}

	/**
	 * Verifica se em alguma c�lula contem um texto espec�fico e compara se dentro
	 * da String do par�metro contem esse texto.
	 * 
	 * @param id_table
	 * @param elemento_busca
	 * @param coluna
	 * @throws Exception
	 */

	public boolean comparaTextoComTextoDaTabela(String id_table, String elemento_busca, int coluna) throws Exception {

		esperaElemento(By.id(id_table));

		List<WebElement> linhas = getDriver()
				.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + coluna + "]"));

		for (int i = 0; i <= linhas.size(); i++) {
			if (elemento_busca.toUpperCase().contains(linhas.get(i).getText().toUpperCase())
					|| linhas.get(i).getText().toUpperCase().contains(elemento_busca.toUpperCase())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Seleciona uma op��o no combobox por texto.
	 * 
	 * @param by
	 * @param texto_selecao
	 * @throws Exception
	 */

	public void selecionaComboTexto(By by, String texto_selecao) throws Exception {
		try {
			esperaElemento(by);
			Select select = new Select(getDriver().findElement(by));
			select.selectByVisibleText(texto_selecao);
		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O TEXTO (" + texto_selecao + "). Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Seleciona uma op��o no combobox por valor.
	 * 
	 * @param by
	 * @param valor_selecao
	 * @throws Exception
	 */

	public void selecionaComboValor(By by, String valor_selecao) throws Exception {
		try {
			esperaElemento(by);
			Select select = new Select(getDriver().findElement(by));
			select.selectByValue(valor_selecao);
		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O VALOR (" + valor_selecao + "). Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Seleciona uma op��o no combobox por ind�ce.
	 * 
	 * @param by
	 * @param indice
	 * @throws Exception
	 */

	public void selecionaComboIndice(By by, int indice) throws Exception {
		try {
			esperaElemento(by);
			Select select = new Select(getDriver().findElement(by));
			select.selectByIndex(indice);
		} catch (Exception e) {
			throw new Exception("ERRO AO SELECIONAR O INDICE (" + indice + "). Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Conta a quantidade de elementos, td, dentro de uma tabela e espera essa
	 * quantidade ser +1.
	 * 
	 * @param td_atual
	 * @param id_tabela
	 * @throws Exception
	 */

	public void esperaTabelaConterUmTdAMais(final int td_atual, final String id_tabela) throws Exception {

		try {

			wait.until(new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driver) {

					try {
						getDSL().waitForAjaxCalls();
						if (contarQtnTdTabelaId(By.id(id_tabela)) > td_atual) {
							return true;
						} else
							return false;
					} catch (Exception e) {
						return false;
					}
				}
			});

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A TABELA CONTAR UMA TD A MAIS. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Espera o frame estar vis�vel e troca usando o indice.
	 * 
	 * @param frame_um
	 * @return o foco j� trocado para o frame de determinado indice.
	 * @throws Exception
	 */

	public boolean esperaETrocaFrame(int frame_um) throws Exception {

		try {

			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame_um));
			return true;

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O FRAME DE INDICE(" + frame_um + ") ESTAR VIS�VEL. Selenium Erro: "
					+ e.getMessage());
		}
	}

	/**
	 * Espera o primeiro frame estar vis�vel e troca, depois espera o segundo frame
	 * estar vis�vel e troca. Ambos usando os indices dos frames.
	 * 
	 * @param frame_um
	 * @param frame_dois
	 * @return o foco j� trocado para o frame de determinado indice.
	 * @throws Exception
	 */

	public boolean esperaETrocaFrame(int frame_um, int frame_dois) throws Exception {

		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame_um));
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame_dois));
			return true;

		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR OS FRAMES DE INDICE(" + frame_um + ") E (" + frame_dois
					+ ") ESTAR VIS�VEL. Selenium Erro: " + e.getMessage());
		}
	}

	public void esperaAlertOuElemento(By by) throws Exception {

		try {
			getWait().until(ExpectedConditions.or(ExpectedConditions.alertIsPresent(),
					ExpectedConditions.invisibilityOfElementLocated(by)));
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO ESPERAR UM ALERT OU UM ELEMENTO SER VISIVEL. Selenium Erro: " + e.getMessage());
		}
	}

	/**
	 * Verifica se o elemento � clic�vel.
	 * 
	 * @param by
	 * @return Se o elemento � clic�vel
	 * @throws Exception
	 */

	public boolean verificaElementoClicavel(By by) throws Exception {

		try {
			getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
			WebElement element = getDriver().findElement(by);
			return element.isEnabled();
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE ELEMENTO E CLICAVEL. Selenium Erro: " + e.getMessage());
		} finally {
			getDriver().manage().timeouts().implicitlyWait(40, TimeUnit.MILLISECONDS);
		}
	}

	public int contaQtnItensSelect(By by) {
		Select slc = new Select(getDriver().findElement(by));
		return slc.getOptions().size();
	}

	public String pegaOpcaoSelecionadaSelect(By by) {
		Select slc = new Select(getDriver().findElement(by));
		List<WebElement> opcoes = slc.getAllSelectedOptions();
		return opcoes.get(0).getText();
	}

	public String pegaWindowHandle() throws Exception {
		try {
			return getDriver().getWindowHandle();
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O ID DA JANELA POR WINDOW HANDLE. Erro Selenium: " + e.toString());
		}
	}

	/**
	 * Espera entre um dos dois elementos passados estarem vis�veis dentro do frame,
	 * condi��o OR.
	 * 
	 * @param by_elementoUm
	 * @param index_um
	 * @param by_elementoDois
	 * @param index_dois
	 */

	public ExpectedCondition<WebElement> esperaElementoSerVisivelFW(final By by) {

		return new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver driver) {
				try {
					getWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
					return driver.findElement(by);
				} catch (Exception e) {
					return null;
				}
			}
		};
	}

	public WebElement verificaVisibilidade(By by) throws Exception {

		try {

			getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
			return getDriver().findElement(by);

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O ELEMENTO ESTA VISIVEL.\r\n" + e.getMessage());
		}

		finally {
			getDriver().manage().timeouts().implicitlyWait(40, TimeUnit.MILLISECONDS);
		}
	}

	public boolean verificaVisibilidadeBoolean(By by) throws Exception {

		try {

			getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
			return getDriver().findElement(by).isDisplayed();

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O ELEMENTO ESTA VISIVEL.\r\n" + e.getMessage());
		} finally {
			getDriver().manage().timeouts().implicitlyWait(40, TimeUnit.MILLISECONDS);
		}
	}

	public void esperaElementoNaoSerVisivel(By by) throws Exception {

		try {
			getWait().until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO NAO SER VISIVEL.\r\n" + e.getMessage());
		}
	}

	public boolean esperaAlert() {

		try {
			getWait().until(ExpectedConditions.alertIsPresent());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean verificaOpcaoExisteComboId(String id_campo, String opcao_texto) throws Exception {

		try {

			List<WebElement> opcoes = getDriver().findElement(By.id(id_campo)).findElements(By.tagName("option"));

			for (WebElement opcao : opcoes) {
				if (opcao.getText().equalsIgnoreCase(opcao_texto))
					return true;
			}

			return false;

		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A OPCAO EXISTE NO COMBO. Selenium Erro: " + e.getMessage());
		}
	}

	/**********************************************************/
	/**********************************************************/
	/**********************************************************/
	/*********************** REFATORAR ***********************/

	/***************************************************************/
	/***************************************************************/

	/******************************************************/
	/************************* ESPERA **********************/

	public WebElement esperaElementoDom(By by) {
		return wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public boolean esperaElementoNaoEstarVisivel(By by) throws Exception {
		try {
			return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
			return false;
		}
	}

	public void esperarElementoPorLoop(By by) throws Exception {

		try {
			WebElement element = getDriver().findElement(by);

			for (int i = 0; i < 10; i++) {
				if (element.isDisplayed()) {
					break;
				} else if (!element.isDisplayed()) {
					Thread.sleep(250);
				}
			}
		} catch (Exception e) {
			throw new Exception("\r\nSelenium Erro: ERRO AO ESPERAR O ELEMENTO ESTAR VISIVEL");
		}

	}

	//

	// Esperar elemento for clicavel - Elemento de Procura ID
	public WebElement esperarElementoForClicavelId(String id_campo) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(id_campo)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + id_campo + ") FOR CLIC�VEL POR ID. Selenium Erro: "
					+ e.toString());
		}
	}

	// Esperar elemento for clicavel - Elemento de Procura ID
	public WebElement esperarElementoForClicavelName(String name_campo) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(name_campo)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + name_campo + ") FOR CLIC�VEL POR NAME. Selenium Erro: "
					+ e.toString());
		}
	}

	// Esperar elemento for clicavel - Elemento de Procura ClassName
	public WebElement esperarElementoForClicavelClassName(String className_campo) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(className_campo)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + className_campo
					+ ") FOR CLIC�VEL POR CLASSNAME. Selenium Erro: " + e.toString());
		}
	}

	// Esperar elemento for clicavel - Elemento de Procura Link text
	public WebElement esperarElementoForClicavelLinkTest(String link_text) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(link_text)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + link_text + ") FOR CLIC�VEL POR LINK. Selenium Erro: "
					+ e.toString());
		}
	}

	// Es�ra elemento for clicavel - Elemento de Procura XPATH
	public WebElement esperarElementoForClicavelXpath(String xpath_campo) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath_campo)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + xpath_campo
					+ ") FOR CLIC�VEL POR XPATH. Selenium Erro: " + e.toString());
		}
	}

	// Esperar elemento estar visivel
	public List<WebElement> esperarElementoEstarVisivelId(String id) throws Exception {
		try {
			return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(id)));
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO ESPERAR O ELEMENTO (" + id + ") ESTAR VIS�VEL POR ID. Selenium Erro: " + e.toString());
		}
	}

	// Esperar Elemento estar Visivel - Elemento de Procura CLASSNAME
	public List<WebElement> esperarElementoEstarVisivelClassName(String className) throws Exception {
		try {
			return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className(className)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + className
					+ ") ESTAR VIS�VEL POR CLASSNAME. Selenium Erro: " + e.toString());
		}
	}

	// Esperar Elemento estar Visivel - Elemento de Procura CLASSNAME
	public List<WebElement> esperarElementoEstarVisivelName(String name) throws Exception {
		try {
			return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name(name)));
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO ESPERAR O ELEMENTO (" + name + ") ESTAR VIS�VEL POR NAME. Selenium Erro: " + e.toString());
		}
	}

	// Esperar Elemento estar Visivel - Elemento de Procura XPATH
	public List<WebElement> esperarElementoEstarVisivelXpath(String xpath) throws Exception {
		try {
			return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + xpath + ") ESTAR VIS�VEL POR XPATH. Selenium Erro: "
					+ e.toString());
		}
	}

	/*******************************************************/
	/*******************************************************/

	/*********************************************************/
	/***********************
	 * VERIFICAR
	 * 
	 * @throws Exception
	 ***********************/

	// public boolean verificarEstaVisivelId(String id_campo) throws Exception {
	// try {
	// getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
	// return getDriver().findElement(By.id(id_campo)).isDisplayed();
	// } catch (Exception e) {
	// return false;
	// }
	// }

	public boolean verificaElementoExiste(By by) throws Exception {
		try {
			getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
			return (getDriver().findElements(by).size() != 0);
		} catch (Exception e) {
			return false;
		}
	}

	// public boolean verificaElementoEstaVisivel(By by) {
	// try {
	// getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
	// return getDriver().findElement(by).isDisplayed();
	// } catch (Exception e) {
	// return false;
	// }
	// }

	public boolean verificarEstaVisivelXpath(String xpath_campo) throws Exception {
		try {
			WebElement elemento = getDriver().findElement(By.xpath(xpath_campo));
			if (elemento.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O ELEMENTO (" + xpath_campo
					+ ") EST� VIS�VEL POR XPATH. Selenium Erro: " + e.toString());
		}
	}

	public boolean verificarCampoContemXpath(String xpath, String valor_esperado) throws Exception {
		try {
			WebElement element = getDriver().findElement(By.xpath(xpath));
			return element.getText().contains(valor_esperado);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O ELEMENTO (" + xpath + ") CONT�M O VALOR (" + valor_esperado
					+ ") POR XPATH. Selenium Erro: " + e.toString());
		}
	}

	public boolean verificarCampoContemTextoId(String id_campo, String texto) {
		WebElement element = getDriver().findElement(By.id(id_campo));
		return element.getText().contains(texto);
	}

	/*********************************************************/
	/*********************************************************/

	/********************************************************/
	/***********************
	 * COMPARAR
	 * 
	 * @throws Exception
	 ***********************/

	public void compararTextoClassName(String className_campo, String texto_esperado) throws Exception {
		try {
			//Assert.assertTrue(
				//	getDriver().findElement(By.className(className_campo)).getText().contains(texto_esperado));
		} catch (Exception e) {
			throw new Exception("ERRO AO COMPARAR O TEXTO DO ELEMENTO (" + className_campo + ") COM O TEXTO ("
					+ texto_esperado + ") POR CLASSNAME. Selenium Erro: " + e.toString());
		}
	}

	public boolean compararTextoId(String id_campo, String texto_esperado) throws Exception {
		try {
			WebElement elemento = getDriver().findElement(By.id(id_campo));
			return elemento.getText().equalsIgnoreCase(texto_esperado);
		} catch (Exception e) {
			throw new Exception("ERRO AO COMPARAR O TEXTO DO ELEMENTO (" + id_campo + ") COM O TEXTO (" + texto_esperado
					+ ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	public void compararTextoXpath(String xpath_campo, String texto_esperado) throws Exception {
		try {
			//WebElement elemento = getDriver().findElement(By.xpath(xpath_campo));
			//String textoElemento = elemento.getText();
			//Assert.assertEquals(textoElemento, texto_esperado);
		} catch (Exception e) {
			throw new Exception("ERRO AO COMPARAR O TEXTO DO ELEMENTO (" + xpath_campo + ") COM O TEXTO ("
					+ texto_esperado + ") POR XPATH. Selenium Erro: " + e.toString());
		}
	}

	public void compararValorId(String id_campo, String valor_esperado) throws Exception {
		try {
			//Assert.assertTrue(getDriver().findElement(By.id(id_campo)).getAttribute("value").contains(valor_esperado));
		} catch (Exception e) {
			throw new Exception("ERRO AO COMPARAR O VALOR DO ELEMENTO (" + id_campo + ") COM O VALOR (" + valor_esperado
					+ ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	/********************************************************/
	/********************************************************/

	/*******************************************************/
	/***********************
	 * JANELAS
	 * 
	 * @throws Exception
	 ***********************/

	// Carrega a pagina (url)
	public void carregarPagina(String url_page) throws Exception {
		try {
			getDriver().get(url_page);
		} catch (Exception e) {
			throw new Exception("ERRO AO CARREGAR A P�IGNA (" + url_page + "). Selenium Erro: " + e.toString());
		}
	}

	// Contar numero de Janelas abertas
	public int numeroJanelasAbertas() throws Exception {
		try {
			String janelas = getDriver().getWindowHandle();
			int numero = janelas.length();
			return numero;
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O N�MERO DE JANELAS ABERTAS. Selenium Erro: " + e.toString());
		}
	}

	// Espera e troca Janela
	public void trocarJanelaFocoCompleto(String url_espera) throws Exception {

		try {
			String[] janelas = getDriver().getWindowHandle().split(",");
			int qtnJanelas = janelas.length + 1;

			if (wait.until(ExpectedConditions.numberOfWindowsToBe(qtnJanelas))) {
				Set<String> janelaVetor = getDriver().getWindowHandles();
				Iterator<String> ite = janelaVetor.iterator();

				while (ite.hasNext()) {
					String window = ite.next().toString();
					getDriver().switchTo().window(window);
					if (getDriver().getCurrentUrl().contains(url_espera)) {
						maximizarJanela();
						break;
					}
				}
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO EFETUAR A TROCA DE JANELA. Selenium Erro: " + e.toString());
		}
	}

	public void trocaJanelaPorUrl(String url) throws Exception {

		Set<String> janelaVetor = getDriver().getWindowHandles();
		Iterator<String> ite = janelaVetor.iterator();

		while (ite.hasNext()) {
			String window = ite.next().toString();
			getDriver().switchTo().window(window);
			if (getDriver().getCurrentUrl().contains(url)) {
				maximizarJanela();
				break;
			}
		}
	}

	// Maximizar Tela
	public void maximizarJanela() throws Exception {
		try {
			if (getDSL().esperaPaginaCarregarJs()) {
				getDriver().manage().window().maximize();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO MAXIMIZAR A JANELA. Selenium Erro: " + e.toString());
		}
	}

	// Fechar Segunda Janela
	public void voltarJanelaPrincipal() throws Exception {
		try {
			getDriver().close();
			getDriver().switchTo().defaultContent();
		} catch (Exception e) {
			throw new Exception("ERRO AO VOLTAR PARA A JANELA PRINCIPAL. Selenium Erro: " + e.toString());
		}
	}

	// Fechar uma Aba
	public void fecharAba() throws Exception {
		try {
			getDriver().close();
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR A ABA DA JANELA. Selenium Erro: " + e.toString());
		}
	}

	/*******************************************************/
	/*******************************************************/

	/*****************************************************/
	/***********************
	 * FRAME
	 * 
	 * @throws Exception
	 ***********************/

	// Pega todos os frames da tela
	public void pegarTodosFrames() throws Exception {
		try {
			List<WebElement> frames = getDriver().findElements(By.tagName("iframe"));
			for (int i = 0; i < frames.size(); i++) {
				System.out.println("Frame " + i + ": " + frames.get(i) + " id= " + frames.get(i).getAttribute("id"));
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO CAPTURAR TODOS OS FRAMES DA JANELA. Selenium Erro: " + e.toString());
		}
	}

	public void trocaFocoIndice(int indice_frame) throws Exception {
		try {
			// getWait().until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(indice_frame));
			// System.out.println("Passou da espera do frame");
			getDriver().switchTo().frame(indice_frame);
		}

		catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O FRAME (" + indice_frame + ") POR INDICE. Selenium Erro: "
					+ e.toString());
		}
	}

	// Volta o foco principal
	public void focoPrincipal() throws Exception {
		try {
			getDriver().switchTo().defaultContent();
		} catch (Exception e) {
			throw new Exception("ERRO AO VOLTAR O FOCO PARA O FRAME PRINCIPAL. Selenium Erro: " + e.toString());
		}
	}

	// Muda o foco para o Alert
	public void focoAlert() throws Exception {
		try {
			getDriver().switchTo().alert();
		} catch (Exception e) {
			throw new Exception("ERRO AO TROCAR O FOCO PARA O ALERT. Selenium Erro: " + e.toString());
		}
	}

	/*****************************************************/
	/*****************************************************/

	/******************************************************/
	/***********************
	 * TABELA
	 * 
	 * @throws Exception
	 ***********************/

	public void clicaTabela(String id_table, int coluna, String elemento_busca) throws Exception {

		try {

			System.out.println("Chegou");

			List<WebElement> linhas = getDriver()
					.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + coluna + "]"));

			for (int i = 1; i <= linhas.size(); i++) {

				if (linhas.get(i).getText().toUpperCase().equals(elemento_busca.toUpperCase())) {

					WebElement cell;

					if (i <= 1) {
						cell = getDriver()
								.findElement(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + coluna + "]"));
					}

					else {
						cell = getDriver().findElement(
								By.xpath("//table[@id='" + id_table + "']//tbody/tr[" + i + "]/td[" + coluna + "]"));
					}

					cell.click();
					break;
				}

			}
			System.out.println("Passou!");

		} catch (Exception e) {
			throw new Exception("Erro: " + e.getMessage());
		}
	}

	public void clicarTabelaDinamica(String id_table, String coluna_busca, String elemento_busca) throws Exception {
		try {
			WebElement tabela = getDriver().findElement(By.xpath("//*[@id='" + id_table + "']"));

			List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
			int idColuna = -1;

			for (int i = 0; i < colunas.size(); i++) {
				if (colunas.get(i).getText().equals(coluna_busca)) {
					idColuna = i + 1;
					break;
				}
			}
			List<WebElement> linhas = getDriver()
					.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + idColuna + "]"));
			int id_linha = -1;
			for (int i = 0; i <= linhas.size(); i++) {
				if (linhas.get(i).getText().equalsIgnoreCase(elemento_busca)) {
					id_linha = i + 2;
					break;
				}
			}
			getDSL().clica(By.xpath("//table[@id='" + id_table + "']/tbody/tr[" + id_linha + "]/td/input"));

		} catch (Exception e) {
			throw new Exception("ERRO AO CLICAR NA TABELA POR XPATH. Selenium Erro: " + e.toString());
		}
	}

	// public void clicarTabelaDinamica(String id_table, int index_coluna, String
	// elemento_busca) throws Exception {
	//
	// WebElement tabela = getDriver().findElement(By.xpath("//*[@id='" + id_table +
	// "']"));
	//
	// List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
	// int idColuna = index_coluna;
	//
	// List<WebElement> linhas = getDriver()
	// .findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" +
	// idColuna + "]"));
	// int id_linha = -1;
	// for (int i = 0; i <= linhas.size(); i++) {
	// if (linhas.get(i).getText().equalsIgnoreCase(elemento_busca)) {
	// id_linha = i + 2;
	// break;
	// }
	// }
	// getDSL().clicarXpath("//table[@id='" + id_table + "']/tbody/tr[" + id_linha +
	// "]/td/input");
	// }

	public boolean verificarSeTabelaContemId(String id_table, String texto) throws Exception {
		try {
			String s = "";
			WebElement table = getDriver().findElement(By.id(id_table));
			List<WebElement> allrows = table.findElements(By.tagName("tr"));

			for (WebElement row : allrows) {
				List<WebElement> Cells = row.findElements(By.tagName("td"));

				for (WebElement Cell : Cells) {
					s = s.concat(Cell.getText());
					if (s.contains(texto)) {
						return true;
					}
				}

			}
			return false;
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A TABELA (" + id_table + ") CONTEM O TEXTO (" + texto
					+ ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	public int localizarLinhaElementoTabelaDinamica(String id_table, String coluna_busca, String elemento_busca)
			throws Exception {
		try {
			WebElement tabela = getDriver().findElement(By.xpath("//*[@id='" + id_table + "']"));

			List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
			int idColuna = -1;

			for (int i = 0; i < colunas.size(); i++) {
				if (colunas.get(i).getText().equals(coluna_busca)) {
					idColuna = i + 1;
				}
			}

			List<WebElement> linhas = getDriver()
					.findElements(By.xpath("//table[@id='" + id_table + "']//tbody/tr/td[" + idColuna + "]"));
			int idLinha = -1;
			for (int i = 0; i <= linhas.size(); i++) {
				if (linhas.get(i).getText().equals(elemento_busca)) {
					idLinha = i + 2;
					break;
				}
			}
			return idLinha;
		} catch (Exception e) {
			throw new Exception("ERRO AO LOCALIZAR O ELEMENTO (" + elemento_busca + ") NA COLUNA (" + coluna_busca
					+ ") DENTRO DA TABELA (" + id_table + ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	public int contaQtnTrTabelaId(By by) throws Exception {
		try {
			esperaElemento(by);
			return getDriver().findElement(by).findElements(By.tagName("tr")).size();

		} catch (Exception e) {
			throw new Exception("ERRO AO CONTAR A QUANTIDADE DE TR DA TABELA.\r\n" + e.getMessage());
		}
	}

	public int contarQtnTdTabelaId(By by) throws Exception {

		try {
			esperaElemento(by);
			return getDriver().findElement(by).findElements(By.tagName("td")).size();
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean verificarColunaContemTextoId(String id_table, String texto, int index_coluna) throws Exception {

		try {

			WebElement tabela = getDriver().findElement(By.xpath("//*[@id='" + id_table + "']"));
			List<WebElement> trList = tabela.findElements(By.xpath(".//tr"));
			int trQtn = trList.size();
			WebElement cedula;

			for (int i = 2; i <= trQtn; i++) {
				cedula = getDriver().findElement(
						By.xpath("//*[@id='" + id_table + "']/tbody/tr[" + i + "]/td[" + index_coluna + "]"));
				if (cedula.getText().contains(texto.toUpperCase())) {
					return true;
				}
			}

			return false;

		} catch (Exception e) {
			throw new Exception(
					"ERRO AO VERIFICAR SE A COLUNA CONTEM O TEXTO (" + texto + "). Selenium Erro: " + e.toString());
		}
	}

	public ExpectedCondition<Boolean> waitForAjaxCalls() {

		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return Boolean.valueOf(((JavascriptExecutor) driver)
						.executeScript("return (window.jQuery != null) && (jQuery.active === 0);").toString());
			}
		};
	}

	/******************************************************/
	/******************************************************/

	/*****************************************************/
	/************************
	 * ALERT
	 **********************/

	public void confirmarAlert() throws Exception {
		esperarAlert();
		getDriver().switchTo().alert().accept();
	}

	public void escreverAlert(String texto) throws Exception {
		try {
			getDSL().esperarAlert();
			getDriver().switchTo().alert().sendKeys(texto);
		} catch (Exception e) {
			throw new Exception("ERRO AO ESCREVER O TEXTO (" + texto + ") NO ALERT. Selenium Erro: " + e.toString());
		}
	}

	public void fecharTodosAlert() throws Exception {
		try {
			while (verificarAlertVisivel()) {
				getDriver().switchTo().alert().accept();
				getDSL().esperarPagina();
				getDSL().waitForAjaxCalls();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR TODOS OS ALERT'S. Selenium Erro: " + e.toString());
		}
	}

	public String pegarTextoAlert() throws Exception {
		try {
			esperarAlert();
			Alert alert = getDriver().switchTo().alert();
			return alert.getText();
		} catch (Exception e) {
			throw new Exception("ERRO AO PEGAR O TEXTO DO ALERT. Selenium Erro: " + e.toString());
		}
	}

	public boolean verificarAlertVisivel() {
		try {
			getDriver().switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	public Alert esperarAlert() throws Exception {
		try {
			return wait.until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ALERT. Selenium Erro: " + e.toString());
		}
	}

	public void cancelarAlert() throws Exception {
		try {
			while (verificarAlertVisivel()) {
				getDriver().switchTo().alert().dismiss();
			}
		} catch (Exception e) {
			throw new Exception("ERRO AO FECHAR TODOS OS ALERT'S. Selenium Erro: " + e.toString());
		}
	}

	/*****************************************************/
	/*****************************************************/

	public void esperarUrlSer(String url) throws Exception {
		try {
			wait.until(ExpectedConditions.urlContains(url));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A URL CONTER (" + url + "). Selenium Erro: " + e.toString());
		}
	}

	/**************************
	 * ESPERAS
	 * 
	 * @throws Exception
	 **********************************/

	// Esperar url conter
	public boolean verificarUrlContem(String url) throws Exception {
		try {
			return getDriver().getCurrentUrl().contains(url);
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE A URL CONTEM (" + url + "). Selenium Erro: " + e.toString());
		}
	}

	// Esperar titulo ser exatamente
	public void verificarTitulo(String texto) throws Exception {
		try {
			//Assert.assertEquals(texto, getDriver().getTitle());
		} catch (Exception e) {
			throw new Exception("ERRO AO VERIFICAR SE O T�TULO � (" + texto + "). Selenium Erro: " + e.toString());
		}
	}

	// // Esperar elemento estar visivel - XPATH
	// public void esperarElementoEstarVisivelXpaths(String xpath_elemento) {
	// try {
	// WebElement elemento = getDriver().findElement(By.xpath(xpath_elemento));
	// wait.until(ExpectedConditions.visibilityOf(elemento));
	// } catch (Exception e) {
	// throw new Exception("ERRO AO VERIFICAR SE O T�TULO � (" + texto + ").
	// Selenium Erro: " + e.toString());
	// }
	// }

	// Espserar elemento ser selecionado
	public Boolean esperarElementoSerSelecionadoId(String id_campo) throws Exception {
		try {
			return wait.until(ExpectedConditions.elementToBeSelected(By.id(id_campo)));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O ELEMENTO (" + id_campo + ") SER SELECIONADO POR ID. Selenium Erro: "
					+ e.toString());
		}
	}

	// Espera Janela ser Localizada
	public boolean esperarJanela() throws Exception {
		try {
			String[] janelas = getDriver().getWindowHandle().split(",");
			int qtnJanelas = janelas.length + 1;
			return wait.until(ExpectedConditions.numberOfWindowsToBe(qtnJanelas));
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR A JANELA ABRIR. Selenium Erro: " + e.toString());
		}
	}
	//
	// public WebDriver esperarFrame(String id_frame) {
	// return
	// wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id(id_frame)));
	// }

	// public boolean esperarFrameIndex(int index) {
	// try {
	// wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(index));
	// return true;
	// } catch (Exception e) {
	// return false;
	// }
	// }

	// Espera FIXA
	public void tempoEsperaFixa(long tempo) throws Exception {
		try {
			Thread.sleep(tempo);
		} catch (Exception e) {
			throw new Exception("ERRO AO ESPERAR O TEMPO (" + tempo + "). Selenium Erro: " + e.toString());
		}
	}

	// Espera pagina carregar
	public void esperarPagina() throws Exception {
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (Exception e) {
			throw new Exception("ERRI AO ESPERAR A P�GINA CARREGAR POR 2 SEGUNDOS. Selenium Erro: " + e.toString());
		}
	}

	/*********************************************************************/

	/******************************
	 * COMANDO
	 * 
	 * @throws Exception
	 ******************************/
	// Comando Enter - Elemento ID
	public void apertarEnterId(String id_campo) throws Exception {
		try {
			getDriver().findElement(By.id(id_campo)).sendKeys(Keys.ENTER);
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO APERTAR ENTER NO ELEMENTO (" + id_campo + ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	// Comando Enter - Elemento NAME
	public void apertarEnterName(String name_campo) throws Exception {
		try {
			getDriver().findElement(By.id(name_campo)).sendKeys(Keys.ENTER);
		} catch (Exception e) {
			throw new Exception(
					"ERRO AO APERTAR ENTER NO ELEMENTO (" + name_campo + ") POR NAME. Selenium Erro: " + e.toString());
		}
	}

	public void apertatShift() {

	}

	public void apertarEnterAlert() throws Exception {
		try {
			getDSL().esperarAlert();
			getDriver().switchTo().alert().accept();
			getDriver().switchTo().defaultContent();
		} catch (Exception e) {
			throw new Exception("ERRO AO APERTAR ENTER NO ALERT. Selenium Erro: " + e.toString());
		}
	}

	public void limparCampoId(String id_campo) throws Exception {
		try {
			getDriver().findElement(By.id(id_campo)).clear();
		} catch (Exception e) {
			throw new Exception("ERRO AO LIMPAR O CAMPO (" + id_campo + ") POR ID. Selenium Erro: " + e.toString());
		}
	}

	/*********************************************************************/

	public void moverParaElementoId(String id_campo) {
		Actions ac = new Actions(getDriver());
		WebElement element = getDriver().findElement(By.id(id_campo));
		ac.moveToElement(element);
	}

}
