package tools;

public class StringEd extends ActionEd {

	/**
	 * Pega uma string e a separa at� o primeiro ponto.
	 * 
	 * @return String at� o primeiro ponto.
	 * @throws Exception
	 */

	public String atePrimeiroPonto(String texto) throws Exception {
		try {
			String[] textoAlterado = texto.split("\\.");
			System.out.println(texto);
			return textoAlterado[0];
		} catch (Exception e) {
			throw new Exception("ERRO AO SEPARAR A STRING ATE O PRIMEIRO PONTO. Java Erro: " + e.toString());
		}
	}
	
	/**
	 * 
	 * @param texto - Texto original a ser separado.
	 * @param separador - Palavra ou caracter respons�vel por ser o separador.
	 * @return - Um vetor de string com as partes que foram separadas.
	 * @throws Exception
	 */
	
	public String[] separaTextoPor(String texto, String separador) throws Exception {
		
		try {			
			return texto.split(separador);			
		} catch(Exception e) {
			throw new Exception("");
		}
		
	}
}
