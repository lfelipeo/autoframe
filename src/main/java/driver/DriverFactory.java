package driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import navegator.models.NavegatorModel;


public class DriverFactory {

	private static WebDriver driver;
	private static WebDriverWait wait;
	private static DSL dsl = null;
	private static NavegatorModel nvg;

	/**
	 * Construtor respons�vel por receber uma classe de navegador.
	 * 
	 * @param nvg
	 *            - Navegador que ser� utilizado.
	 */

	public DriverFactory(NavegatorModel nvg_parameter) { 
		nvg = nvg_parameter;
	}

	public DriverFactory() {

	}

	public WebDriver getDriver() {

		if (driver == null) {

			nvg.setPropriedades();
			driver = nvg.webDriver();

			driver.manage().window().maximize();

			driver.manage().timeouts().setScriptTimeout(1, TimeUnit.MINUTES);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
		}

		return driver;
	}

	public WebDriverWait getWait() {
		wait = new WebDriverWait(getDriver(), 40);
		wait.ignoring(UnhandledAlertException.class).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		return wait;
	}

	public DSL getDSL() {
		if (dsl == null) {
			dsl = new DSL();
		}
		return dsl = new DSL();
	}

	protected void voltaTempoDeEspera() {
		driver.manage().timeouts().setScriptTimeout(1, TimeUnit.MINUTES);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
	}
}
