package navegator.models;

import org.openqa.selenium.ie.InternetExplorerDriver;


/**
 * Classe referente ao navegador Internet Explore.
 * 
 * @author LFO
 * @since 09/08/2018
 */

public class IEModel extends NavegatorModel {

	public IEModel() {
		getName();
		getPath();
	}
	
	//private static final String pathInternetExploreJson = "navegator\\config\\IEConfig.json";

	//private static JsonObj iejs = new JsonObj(new File(pathInternetExploreJson));

	public String getName() {
		return ("webdriver.ie.driver");
	}

	public String getPath() {
		return ("Driver/IEDriverServer.exe");
	}

	@Override
	public InternetExplorerDriver webDriver() {
		return new InternetExplorerDriver();
	}

	@Override
	public void setPropriedades() {
		System.setProperty(getName(), getPath());
	}

}
