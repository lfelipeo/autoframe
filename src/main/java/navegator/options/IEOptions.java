package navegator.options;

import java.util.logging.Level;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class IEOptions extends InternetExplorerOptions {

	private static final long serialVersionUID = 1L;

	private DesiredCapabilities cap;
	private LoggingPreferences loggingPrefs;

	public IEOptions() {
		this.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		this.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		this.setCapability(InternetExplorerDriver.ELEMENT_SCROLL_BEHAVIOR, true);
		this.setCapability("marionette", false);
		this.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
		this.setCapability(CapabilityType.VERSION, "11");
		this.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
		this.merge(getCap());
	}

	public DesiredCapabilities getCap() {
		cap = new DesiredCapabilities();
		cap.setCapability("marionette", false);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(CapabilityType.LOGGING_PREFS, getLogginPrefs());
		cap.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
		return cap;
	}

	public LoggingPreferences getLogginPrefs() {
		loggingPrefs = new LoggingPreferences();
		loggingPrefs.enable(LogType.BROWSER, Level.ALL);
		loggingPrefs.enable(LogType.CLIENT, Level.ALL);
		loggingPrefs.enable(LogType.DRIVER, Level.ALL);
		loggingPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		loggingPrefs.enable(LogType.PROFILER, Level.ALL);
		loggingPrefs.enable(LogType.SERVER, Level.ALL);
		return loggingPrefs;
	}

}
