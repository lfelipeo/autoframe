package json;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonObj extends JSONObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static JSONObject json = new JSONObject();
	private JSONParser jParser = new JSONParser();
	private static FileWriter writeFile = null;

	private static File path;

	public JsonObj(File file) {

		try {

			if (!file.exists()) {
				throw new Exception("Json Erro: O ARQUIVO NAO EXISTE");
			}

			path = file;

		} catch (Exception e) {

		}
	}

	public JSONObject getJson() {
		return json;
	}

	public JSONParser getJParser() {
		return jParser;
	}

	public String get(String atributo) {

		try {
			json = (JSONObject) jParser.parse(new FileReader(path));
			return (String) json.get(atributo);
		} catch (Exception e) {
			return null;
		}
	}

	public static void criaJson(String nome_arquivo) throws Exception {

		try {

			File arquivoJson = new File("json/" + nome_arquivo + ".json");
			if (!arquivoJson.exists()) {
				arquivoJson.createNewFile();
			}

		} catch (Exception e) {
			throw new Exception("ERRO AO CRIAR UM ARQUIVO JSON. Json Erro: " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public static void escreveJson(File file, String atributo, String valor) {
		try {
			json.put(atributo, valor);
			writeFile = new FileWriter(file);
			writeFile.write(json.toString());
			writeFile.close();
		} catch (IOException e1) {
		}
	}

}
