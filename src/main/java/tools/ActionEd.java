package tools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionEd extends Javascript {

	private Actions ac;

	public void doisClique(By by) throws Exception {
		try {
			WebElement element = getDriver().findElement(by);
			ac.doubleClick(element);
		} catch (Exception e) {
			throw new Exception("ERRO AO EFETUAR UM DOIS CLIQUES. Action Erro: " + e.getMessage());
		}
	}
}
